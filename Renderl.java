package snake;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

import javax.swing.JPanel;

@SuppressWarnings("serial")

public class Render extends JPanel {

	public static final Color GREEN = new Color(1666073);

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		Snake snake = Snake.snake;

		g.setColor(GREEN);

		g.fillRect(0, 0, 805, 700);

		g.setColor(Color.WHITE);

		for (Point point : snake.snakeParts) {
			g.fillRect(point.x * Snake.SCALE, point.y * Snake.SCALE, Snake.SCALE, Snake.SCALE);
		}

		g.fillRect(snake.head.x * Snake.SCALE, snake.head.y * Snake.SCALE, Snake.SCALE, Snake.SCALE);

		g.setColor(Color.RED);

		g.fillRect(snake.apple.x * Snake.SCALE, snake.apple.y * Snake.SCALE, Snake.SCALE, Snake.SCALE);
		
		g.setColor(Color.PINK);
		
		g.fillRect(snake.bonus.x * Snake.SCALE, snake.bonus.y * Snake.SCALE, Snake.SCALE, Snake.SCALE);

		String string = "Score: " + snake.score + ", Length: " + snake.tailLength + ", Time: " + snake.time / 20;

		g.setColor(Color.white);

		g.drawString(" Move with W,A,S,D.   " + "To start a new game or pause press ESCAPE", 200, 30);

		g.setColor(Color.white);

		g.drawString(string, (int) (getWidth() / 2 - string.length() * 2.5f), 10);

		string = "Game Over!";

		if (snake.over) {
			g.drawString(string, (int) (getWidth() / 2 - string.length() * 2.5f), (int) snake.dim.getHeight() / 4);
		}

		string = "PAUSED!";

		if (snake.paused && !snake.over) {
			g.drawString(string, (int) (getWidth() / 2 - string.length() * 2.5f), (int) snake.dim.getHeight() / 4);
		}
	}
}
